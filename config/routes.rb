Rails.application.routes.draw do
  namespace :api do
    get 'android/index'
    get 'android/show'
    get 'android/used_coupons'
    get 'android/invitees'
    get 'android/friends'
  end

  devise_for :admins, controllers: { sessions: 'sessions', registrations: 'registrations'}
  namespace :api do
    scope '/v1' do
      resource :sessions, only: [:create] do
        post :request_phone_verification_token
      end

      resource :third_party_accounts, only: :create

      resource :registrations, only: [:create]

      resources :coupons, only: [:index, :show] do
        resources :comments, only: [:index, :create]
        collection do
          get :hot_and_nearby
          get :most_nearby
          get :best_price
          get :best_service
        end

        resources :users, only: [] do
          member do
            get :invitees
          end
        end
      end

      resources :follows, only: [:create] do
        collection do
          delete :destroy
        end
      end

      resources :users, only: [:show, :update] do
        member do
          get :available_coupons
          get :used_coupons
          get :friends
          post :email_registration
          post :email_verification
          post :resend_email_verification
        end
      end

      resources :user_coupons , only: [:create, :update, :show] do
        member do
          get :get_user_coupon
        end
      end
      

      resources :coupon_invitations, only: [:create, :update]

      resource :interactivity_statuses, only: [:index] do
        post :index
      end

      resources :venues, only: [:index, :show] do
        collection do
          get :popular_restaurants
        end
      end

      resources :restaurants, only: [:index, :show] do
        resources :comments, only: [:index, :create]
      end

      resources :envs, only: [:index]

      resources :restaurant_categories do
        member do
          get :restaurants
        end
      end
    end
  end

  namespace :cms do
    resources :coupons
    resources :venues do
      get :photos
      get :reviews
    end
    resources :restaurants
    resources :users
    resources :advertising_events
    resources :partners
    resources :restaurant_categories
    resources :admins, controller: :restaurant_owners, path: :restaurant_owners
    resources :menus
  end
  
  namespace :owner do
    resources :facebook_fanpages
    resources :coupons do
      collection do
        get :code_form
        post :redeem_code
      end
    end
    resources :restaurants

    resources :third_party_accounts, only: [:index] do
      collection do
        get 'page_list'
        post 'sync_data_from_facebook_account'
      end
    end

    resources :advertising_events
    resources :restaurants do
      collection do
        get :connect_a_restaurant
        post :restaurant_reviews
      end
    end
    resources :menus
  end

  get '/auth/facebook'
  get 'auth/:provider/callback' => 'omniauth#omniauth_callback'
  get 'auth/failure' => 'omniauth#omniauth_fallback'
  
  # resources :advertising_events, only: [:show] do
  #   collection do
  #     post '/register_phone' => 'advertising_events#register_phone'
  #     post '/verify_phone' => 'advertising_events#verify_phone'
  #     post '/resend_phone_token' => 'advertising_events#resend_phone_token'
  #     get '/reward' => 'advertising_events#reward'
  #     get '/phone_registration_page' => 'advertising_events#phone_registration_page'
  #     get '/thank_you' => 'advertising_events#thank_you'
  #   end
  # end
  
  namespace :facebook do
    get '/voucher', to: 'vouchers#index'
    post '/voucher', to: 'vouchers#index'
    get '/voucher/index', to: 'vouchers#index'
    post '/voucher', to: 'vouchers#index'
    post '/voucher/registration', to: 'vouchers#registration'
    post '/voucher/sendsms', to: 'vouchers#send_sms'
    get '/startpage', to: 'vouchers#startpage'
    get '/voucher/restaurant-ad-event', to: 'vouchers#restaurant_advertising_event'
    post '/voucher/restaurant-ad-event', to: 'vouchers#restaurant_advertising_event'
    get '/menu', to: 'menu_tabs#index'
    post '/menu', to: 'menu_tabs#index'
  end

  root 'static_pages#index'
  get 'service-intro' => 'static_pages#service_intro'
  get 'app-intro' => 'static_pages#app_intro'
  get 'index' => 'static_pages#index'
  get 'voucher' => 'static_pages#voucher'
end
