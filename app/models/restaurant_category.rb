class RestaurantCategory < ActiveRecord::Base
  has_many :restaurants
  mount_uploader :image, ImageUploader

  def restaurant_count
    #this will be change later when table 'places' is renamed to restaurants later 
    restaurants.count
  end
end