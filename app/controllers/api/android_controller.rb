class Api::AndroidController < ActionController::Base
  def index
    @usr = User.find_by_id(9)
    render json: @usr
  end

  def show
    @usr = User.find_by_id(9)
    render json: @usr
  end
  
  def used_coupons
      @usr = User.find_by_id(params[:id])
      render json: ArrayPresenter.new(@usr.used_user_coupons, UserCouponPresenter)
  end
  
  def invitees
    @coupon = Coupon.find_by_id(params[:coupon_id])
    @usr = User.find_by_id(params[:user_id])
    pending_invitees = ArrayPresenter.new(@usr.pending_invitees(@coupon), UserPresenter)
    accepted_invitees = ArrayPresenter.new(@usr.accepted_invitees(@coupon), UserPresenter)
    denied_invitees = ArrayPresenter.new(@usr.denied_invitees(@coupon), UserPresenter)
    render json: { pending_invitees: pending_invitees, accepted_invitees: accepted_invitees, denied_invitees: denied_invitees }
    #render json: { pending_invitees: pending_invitees }
  end
  
  def friends    
    if @usr = User.find_by_id(params[:id])
      
      #if @usr.following_users.count > 0
     #   @followings = ArrayPresenter.new(@usr.following_users, UserPresenter)    
     # end
      #if @usr.user_followers.count > 0
        #@followers = ArrayPresenter.new(@usr.user_followers, UserPresenter)    
     # end     
      render json: { followings: @usr.following_users , followers: @usr.user_followers}
      #render json: @followings
    else
      render_errors(I18n.t("base.api.not_found"), :not_found)
    end
  end
  
end
