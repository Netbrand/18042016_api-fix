module Api
  class CouponInvitationPresenter < Presenter
    def as_json(*)
      {
        id: object.id,
        invitee: object.invitee.representative_name,
        invitee_id: object.invitee.id,
        coupon_id: object.coupon.id,
        coupon_code: object.coupon.code,
        status: object.status
      }
    end
  end
end
