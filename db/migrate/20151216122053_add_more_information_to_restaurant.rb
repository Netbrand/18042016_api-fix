class AddMoreInformationToRestaurant < ActiveRecord::Migration
  def change
    add_column :restaurants, :should_reserve_table, :boolean #Nên đặt trước
    add_column :restaurants, :delivery, :boolean #Có giao hàng
    add_column :restaurants, :bring_home, :boolean #Cho mua về
    add_column :restaurants, :has_wifi, :boolean #Có wifi
    add_column :restaurants, :has_kid_zone, :boolean #Có chỗ chơi cho trẻ em
    add_column :restaurants, :has_table_outside, :boolean #Có bàn ngoài trời
    add_column :restaurants, :has_private_room, :boolean #Có phòng riêng
    add_column :restaurants, :has_air_conditioner, :boolean #Có máy lạnh
    add_column :restaurants, :has_scard_payment, :boolean #Trả bằng thẻ: Visa/Master
    add_column :restaurants, :has_karaoke, :boolean #Có karaoke
    add_column :restaurants, :has_free_motobike_parking, :boolean #Giữ xe máy miễn phí
    add_column :restaurants, :give_tip, :boolean #Tip cho nhân viên
    add_column :restaurants, :has_car_parking, :boolean #Có chỗ đậu ôtô
    add_column :restaurants, :has_smoking_area, :boolean #Có khu vực hút thuốc
    add_column :restaurants, :has_member_card, :boolean #Có thẻ thành viên
    add_column :restaurants, :has_offical_receipt, :boolean #Có xuất hóa đơn đỏ
    add_column :restaurants, :has_reference_support, :boolean #Có hỗ trợ hội thảo
    add_column :restaurants, :has_heater, :boolean #Có lò sưởi
    add_column :restaurants, :has_disable_support, :boolean #Có hỗ trợ người khuyết tật
    add_column :restaurants, :has_football_screen, :boolean #Có chiếu bóng đá
    add_column :restaurants, :has_live_music, :boolean #Có nhạc sống
    add_column :restaurants, :description, :text #Miêu tả
    add_column :restaurants, :price_range, :string #Dưới 200k = $, Từ 200 - 500k = $$, từ 500 - 1triệu = $$$, trên 1 triệu = $$$$)
    add_column :restaurants, :food_menu, :string #Phục vụ món: (tên món)
    add_column :restaurants, :open_at, :string
    add_column :restaurants, :close_at, :string
    add_column :restaurants, :average_menu_price, :integer #gia trung binh thuc don
    add_column :restaurants, :food_style, :string #Phong cách ẩm thực: Châu Á,Tây Âu, Trung quốc
    add_column :restaurants, :using_purpose, :string #Mục đích sử dụng: Đãi tiệc,Ăn gia đình,Hẹn hò,Họp nhóm,Tiếp khách
    add_column :restaurants, :capacity, :integer #người lớn(Có chỗ cho trẻ em)
    add_column :restaurants, :suitable_time, :string #Thích hợp: Buổi sáng,Buổitrưa,Buổi xế,Buổitối
    add_column :restaurants, :extra_fee, :string #Phí dịch vụ tính thêm:
    add_column :restaurants, :time_to_book_a_table, :string #Đặt bàn trước: Khoảng 3 - 5 phút
    add_column :restaurants, :website, :string
    add_column :restaurants, :facebook_url, :string
    #Đánh giá: Tài khoản mới bắt đầu đánh giá 5.0/10
    #Follower: Số lượt người theo dõi (đã tương tác like, bình luận, thích)
    #Like: Số lượt ngươi thích trên App
    #Bình luận: Chia sẻ 
  end
end
