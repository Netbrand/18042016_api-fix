class CreateMenuComponents < ActiveRecord::Migration
  def change
    create_table :menu_components do |t|

      t.string :content
      t.integer :menu_id
      t.timestamps null: false
    end
  end
end
