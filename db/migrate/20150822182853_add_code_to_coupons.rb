class AddCodeToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :code, :string, index: true
  end
end
