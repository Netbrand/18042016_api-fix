class AddSectionToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :section, :string
  end
end
