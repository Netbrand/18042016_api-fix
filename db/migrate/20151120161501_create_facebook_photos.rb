class CreateFacebookPhotos < ActiveRecord::Migration
  def change
    create_table :facebook_photos do |t|

      t.string :url
      t.integer :venue_id
      t.timestamps null: false
    end
  end
end
