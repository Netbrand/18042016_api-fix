class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|

      t.string :name
      t.string :category
      t.integer :price
      t.integer :owner_id
      t.string :owner_type
      t.string :image
      t.timestamps null: false
    end
  end
end
