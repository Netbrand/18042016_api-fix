class RemoveVerifiedFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :verified_at
    remove_column :users, :verification_sent_at
  end
end
