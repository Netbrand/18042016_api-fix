class AddMoreAttributeToAdvertisingEvent < ActiveRecord::Migration
  def change
    add_column :advertising_events, :enabled, :boolean, default: true
    add_column :advertising_events, :reference_link_1, :text
    add_column :advertising_events, :reference_link_2, :text
  end
end
