class AddSlugToAdvertisingEvent < ActiveRecord::Migration
  def change
    add_column :advertising_events, :url, :string
    add_index :advertising_events, :slug
  end
end
