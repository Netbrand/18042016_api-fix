class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :opening_hours
      t.string :price
      t.string :food_category
      t.string :restaurant_category
      t.string :target_customer
      t.string :category
      t.string :cooking_time
      t.text :website
      t.boolean :online_order
      t.timestamps null: false
    end
  end
end

# Tên nhà hàng
# Địa chỉ
# Giờ mở cửa 08:15 AM - 10:00 PM
# Khoảng giá: Giá trung bình trên menu
# PhanLoaiMonAn: Châu Á,Tây Âu, Trung quốc
# PhanLoaiNhaHang: Sang trọng,Nhà hàng...
# PhuHopVoi: Đãi tiệc,Ăn gia đình,Hẹn hò,Họp nhóm,Tiếp khách
# Sức chưa: 100 người lớn(Có chỗ cho trẻ em)
# Tên nhà hàng: Eureka Coffee & Fastfood
# TheLoai: Buổi sáng,Buổitrưa,Buổi xế,Buổitối
# Phí dịch vụ tính thêm: 
# ThoiGianChuanBi: Khoảng 3 - 5 phút
# WebSite: http://www.tiecvn.com
# Facebook: 
# Đánh giá: Tài khoản mới bắt đầu đánh giá 5.0/10
# Follower: Số lượt người theo dõi (đã tương tác like, bình luận, thích)
# Like: Số lượt ngươi thích trên App 
# Bình luận:
# Chia sẻ 
# Đặt bàn Online