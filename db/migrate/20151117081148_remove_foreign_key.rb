class RemoveForeignKey < ActiveRecord::Migration
  def change
    remove_foreign_key :identities, column: :user_id
  end
end
