class AddAdvertisingEventIdToUserAdvertisingEvent < ActiveRecord::Migration
  def change
    add_column :user_advertising_events, :advertising_event_id, :integer
    add_index :user_advertising_events, :advertising_event_id
    remove_column :user_advertising_events, :is_verified
    remove_column :user_advertising_events, :event_name
  end
end
