class AddProfileImageToRestaurant < ActiveRecord::Migration
  def change
    add_column :restaurants, :profile_image, :string
  end
end
