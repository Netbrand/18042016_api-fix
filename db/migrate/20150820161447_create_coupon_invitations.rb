class CreateCouponInvitations < ActiveRecord::Migration
  def change
    create_table :coupon_invitations do |t|
      t.integer :inviter_id, index: true
      t.integer :invitee_id, index: true
      t.references :coupon, index: true, foreign_key: true
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
