class AddMoreInformationToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :crawled?, :boolean, default: false
    create_table :reviews do |t|
      t.string :text
      t.integer :venue_id
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :reviews, :user_id
    add_index :reviews, :venue_id
  end
end
