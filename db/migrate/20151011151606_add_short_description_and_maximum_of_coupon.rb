class AddShortDescriptionAndMaximumOfCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :short_description, :string
    add_column :coupons, :quantity, :integer, default: 10
  end
end
