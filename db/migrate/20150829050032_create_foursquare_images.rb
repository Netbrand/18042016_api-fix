class CreateFoursquareImages < ActiveRecord::Migration
  def change
    create_table :foursquare_images do |t|
      t.string :url
      t.integer :venue_id

      t.timestamps null: false
    end
  end
end
