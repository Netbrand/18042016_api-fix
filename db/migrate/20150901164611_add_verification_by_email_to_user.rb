class AddVerificationByEmailToUser < ActiveRecord::Migration
  def change
    add_column :users, :unverified_email, :string
    add_column :users, :email_verification_token, :string
  end
end
