class AddOwnerIdToVenue < ActiveRecord::Migration
  def change
    add_column :venues, :owner_id, :integer
    add_index :venues, :owner_id
  end
end
