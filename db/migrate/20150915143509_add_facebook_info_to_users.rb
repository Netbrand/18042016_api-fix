class AddFacebookInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :facebook_token, :text
    add_column :users, :facebook_token_expiration, :integer
    add_column :users, :facebook_image_url, :text
  end
end
