class CreateRestaurantWaitListReviews < ActiveRecord::Migration
  def change
    create_table :restaurant_wait_list_reviews do |t|
      t.integer :restaurant_id
      t.integer :admin_id
      t.integer :status, default: 0

      t.timestamps null: false
    end
  end
end
