class CreateFoodyComments < ActiveRecord::Migration
  def change
    create_table :foody_comments do |t|

      t.integer :place_id
      t.integer :user_id
      t.string :user_name
      t.string :user_avatar
      t.string :title
      t.string :message
      t.integer :time
    end
  end
end
