class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :phone
      t.string :avatar

      t.string   :verification_token
      t.datetime :verified_at
      t.datetime :verification_sent_at

      t.timestamps null: false
    end
    add_index :users, :phone, unique: true
  end
end
