class AddForeignKeyAdvertisingEventAdmin < ActiveRecord::Migration
  def change
    add_column :advertising_events, :user_id, :integer
    add_index :advertising_events, :user_id
    add_foreign_key :advertising_events, :admins, index: true, column: :user_id, on_delete: :cascade
  end
end
