class AddNumberOfFreeVolkaToCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :number_of_free_volka, :integer, default: 0
  end
end
