class AddTablesForFoodyData < ActiveRecord::Migration
  def change
    drop_table :restaurants
    create_table :cities do |t|
      t.string :alias
      t.string :name
      t.integer :position
    end

    create_table :districts do |t|
      t.integer :city_id
      t.string :alias
      t.string :name
      t.integer :position
    end

    create_table :areas do |t|
      t.integer :city_id
      t.integer :district_id
      t.string :alias
      t.string :name
      t.integer :position
    end
    add_index :areas, :city_id
    add_index :areas, :district_id

    create_table :restaurant_categories do |t|
      t.string :alias
      t.string :name
      t.integer :position
    end

    create_table :places do |t|
      t.integer :city_id
      t.integer :district_id
      t.integer :area_id
      t.integer :restaurant_category_id
      t.string :alias
      t.string :name
      t.string :address
      t.string :image
      t.string :phone
      t.float :lat
      t.float :lon
    end
    add_index :places, :city_id
    add_index :places, :district_id
    add_index :places, :area_id
  end
end
