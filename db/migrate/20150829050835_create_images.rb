class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :content
      t.integer :object_id
      t.string  :object_type

      t.timestamps null: false
    end
  end
end
