class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      t.text :content
      t.integer :rate
      t.references :commentable, polymorphic: true
      t.references :commenter, polymorphic: true
      t.timestamps
    end

    add_index :comments, :commentable_type
    add_index :comments, :commentable_id
    add_index :comments, :commenter_type
    add_index :comments, :commenter_id
  end

  def self.down
    drop_table :comments
  end
end
