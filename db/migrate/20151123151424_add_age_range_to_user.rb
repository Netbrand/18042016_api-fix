class AddAgeRangeToUser < ActiveRecord::Migration
  def change
    add_column :users, :age_range, :string
    add_column :users, :location, :string
  end
end
