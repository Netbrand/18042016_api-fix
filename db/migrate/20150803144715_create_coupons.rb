class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :image
      t.text :description
      t.datetime :start_date
      t.datetime :end_date
      t.references :venue, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
