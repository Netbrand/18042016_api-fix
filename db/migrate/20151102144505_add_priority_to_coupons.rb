class AddPriorityToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :priority, :integer, default: 0
  end
end
