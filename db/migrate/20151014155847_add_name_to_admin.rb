class AddNameToAdmin < ActiveRecord::Migration
  def change
    add_column :admins, :name, :string
    rename_column :venues, :owner_id, :admin_id
  end
end
