class AddCouponDiscountDetails < ActiveRecord::Migration
  def change
    add_column :coupons, :cash_discount, :string
    add_column :coupons, :bill_discount, :string
    add_column :coupons, :food_discount, :string
  end
end
