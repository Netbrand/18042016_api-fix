class CreatePartners < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :name
      t.text :url
      t.string :image
      t.boolean :active
      t.integer :order

      t.timestamps null: false
    end
  end
end
