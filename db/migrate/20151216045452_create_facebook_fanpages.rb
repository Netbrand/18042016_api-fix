class CreateFacebookFanpages < ActiveRecord::Migration
  def change
    create_table :facebook_fanpages do |t|
      t.string :name
      t.integer :admin_id

      t.timestamps null: false
    end
  end
end
