class CreateServerConfigurations < ActiveRecord::Migration
  def change
    create_table :server_configurations do |t|
      t.string :value
      t.string :name

      t.timestamps null: false
    end
  end
end
