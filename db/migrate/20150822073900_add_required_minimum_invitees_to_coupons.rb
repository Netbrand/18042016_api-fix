class AddRequiredMinimumInviteesToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :required_minimum_invitees, :integer, default: 2
  end
end
