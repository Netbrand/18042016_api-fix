class RemoveVenueCategoriesTable < ActiveRecord::Migration
  def up
    drop_table :venue_categories
  end

  def down
    create_table :venue_categories do |t|
      t.string :category_name
      t.integer :venue_id

      t.timestamps null: false
    end
  end
end
