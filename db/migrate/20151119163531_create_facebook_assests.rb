class CreateFacebookAssests < ActiveRecord::Migration
  def change
    create_table :facebook_assests do |t|
      t.integer :venue_id
      t.string :information

      t.timestamps null: false
    end

    add_index :facebook_assests, :venue_id
  end
end
