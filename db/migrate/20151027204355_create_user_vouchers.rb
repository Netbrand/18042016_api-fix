class CreateUserVouchers < ActiveRecord::Migration
  def change
    create_table :user_vouchers do |t|
      t.references :user, index: true, foreign_key: true
      t.references :coupon, index: true, foreign_key: true
      t.integer :status
      t.string :code

      t.timestamps null: false
    end
  end
end
