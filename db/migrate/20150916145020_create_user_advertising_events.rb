class CreateUserAdvertisingEvents < ActiveRecord::Migration
  def change
    create_table :user_advertising_events do |t|
      t.integer :user_id
      t.boolean :is_verfied, default: true
      t.string :event_name
      t.string :lucky_code

      t.timestamps null: false
    end

    add_index :user_advertising_events, :user_id
  end
end
