class CreateFoodyImages < ActiveRecord::Migration
  def change
    create_table :foody_images do |t|
      t.integer :place_id
      t.integer :album_id
      t.string :url
    end
  end
end
