class CreateVenues < ActiveRecord::Migration
  def change
    create_table :venues do |t|
      t.string :name
      t.text :description
      t.float :longitude
      t.float :latitude

      t.timestamps null: false
    end
  end
end
