class ChangeUserAgentToUseEnunm < ActiveRecord::Migration
  def change
    change_column :devices, :user_agent, 'integer USING CAST(user_agent AS integer)', default: 0
  end
end
