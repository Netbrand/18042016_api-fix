class PartnerSetDefaultValueForActive < ActiveRecord::Migration
  def change
    change_column :partners, :active, :boolean, default: true
  end
end
