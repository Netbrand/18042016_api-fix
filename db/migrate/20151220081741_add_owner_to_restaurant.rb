class AddOwnerToRestaurant < ActiveRecord::Migration
  def change
    add_column :restaurants, :owner_id, :integer
    add_column :restaurants, :owner_type, :string
  end
end
