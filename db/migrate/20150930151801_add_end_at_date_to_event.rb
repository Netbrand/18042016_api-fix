class AddEndAtDateToEvent < ActiveRecord::Migration
  def change
    add_column :advertising_events, :end_at, :datetime
    add_column :advertising_events, :begin_at, :datetime
  end
end
