class CreateMenuCategories < ActiveRecord::Migration
  def up
    create_table :menu_categories do |t|
      t.string :name
      t.timestamps null: false
    end

    remove_column :menus, :category
    add_column :menus, :menu_category_id, :integer
    MenuCategory.create(id: 1, name: MenuCategory::BREAKFIRST)
    MenuCategory.create(id: 2, name: MenuCategory::LUNCH)
    MenuCategory.create(id: 3, name: MenuCategory::DINNER)
  end

  def down
    drop_table :menu_categories
    remove_column :menus, :menu_category_id
    add_column :menus, :category, :string
  end
end
