class RemoveForiegnKeyForCoupon < ActiveRecord::Migration
  def change
    rename_column :coupons, :restaurant_id, :venue_id
    remove_foreign_key :coupons, :venue
    add_column :coupons, :restaurant_id, :integer
    remove_column :coupons, :venue_id
    add_foreign_key :coupons, :restaurants, column: :restaurant_id
  end
end
