class AddMoreInformationToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :fd_id, :integer
    add_column :places, :complete, :integer
  end
end
