class AddTokenToUserCoupon < ActiveRecord::Migration
  def change
    add_column :user_coupons, :token, :string
  end
end
