class CreateVenueCategories < ActiveRecord::Migration
  def change
    create_table :venue_categories do |t|
      t.integer :venue_id
      t.string :category_name

      t.timestamps null: false
    end

    remove_column :venues, :section
    add_index :venue_categories, :venue_id
    add_index :venue_categories, :category_name
  end
end
