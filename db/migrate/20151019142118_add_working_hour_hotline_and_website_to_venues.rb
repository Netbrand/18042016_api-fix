class AddWorkingHourHotlineAndWebsiteToVenues < ActiveRecord::Migration
  def change
    add_column :venues, :opening_hours, :text
    add_column :venues, :website, :text
    add_column :venues, :formatted_phone_number, :string
  end
end
