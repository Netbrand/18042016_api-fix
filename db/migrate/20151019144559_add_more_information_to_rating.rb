class AddMoreInformationToRating < ActiveRecord::Migration
  def change
    add_column :reviews, :rating, :integer
    add_column :reviews, :author_name, :string
  end
end
