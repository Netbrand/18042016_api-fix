class AddDefaultPriceToCoupons < ActiveRecord::Migration
  def change
    change_column :coupons, :cash_discount, :string, default: '0'
    change_column :coupons, :bill_discount, :string, default: '0'
  end
end
