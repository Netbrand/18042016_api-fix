class MoveToUserGooglePlace < ActiveRecord::Migration
  def change
    remove_column :venues, :foursquare_id
    drop_table :foursquare_images
    add_column :venues, :google_place_id, :string
    add_column :venues, :google_place_reference, :text

    create_table :google_place_images do |t|
      t.string :url
      t.integer :venue_id

      t.timestamps null: false
    end

    add_index :google_place_images, :venue_id
  end
end
