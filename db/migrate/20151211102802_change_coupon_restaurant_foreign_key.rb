class ChangeCouponRestaurantForeignKey < ActiveRecord::Migration
  def change
    rename_column :coupons, :venue_id, :restaurant_id
  end
end
