class AddImageToRestaurantCategory < ActiveRecord::Migration
  def change
    add_column :restaurant_categories, :image, :string
  end
end
