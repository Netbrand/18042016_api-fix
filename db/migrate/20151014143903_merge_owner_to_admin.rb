class MergeOwnerToAdmin < ActiveRecord::Migration
  def up
    drop_table :restaurant_owners
    add_column :admins, :role, :string, default: Admin::OWNER
  end

  def down
    remove_column :admins, :role
  end
end
