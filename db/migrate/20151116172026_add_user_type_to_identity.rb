class AddUserTypeToIdentity < ActiveRecord::Migration
  def change
    add_column :identities, :user_type, :string, default: 'User' 
  end
end
