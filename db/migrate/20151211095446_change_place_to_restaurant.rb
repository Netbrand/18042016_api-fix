class ChangePlaceToRestaurant < ActiveRecord::Migration
  def change
    rename_table :places, :restaurants
    rename_column :foody_comments, :place_id, :restaurant_id
    rename_column :foody_images, :place_id, :restaurant_id
  end
end
