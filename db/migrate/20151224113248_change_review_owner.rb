class ChangeReviewOwner < ActiveRecord::Migration
  def change
    rename_column :restaurant_wait_list_reviews, :admin_id, :facebook_fanpage_id
  end
end
