class AddImpressionsCountToCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :impressions_count, :integer, default: 0
  end
end
