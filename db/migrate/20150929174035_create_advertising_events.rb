class CreateAdvertisingEvents < ActiveRecord::Migration
  def change
    create_table :advertising_events do |t|
      t.string :name
      t.string :description
      t.string :slug
      t.string :home_page_background
      t.string :win_page_background
      t.float :winning_rate
      t.integer :maximum_number_of_winners

      t.timestamps null: false
    end
  end
end
